require "./merger"
require "option_parser"
require "colorize"

main_script = "main.lua"
output = STDOUT
parser = OptionParser.parse! do |parser|
  parser.banner = "Usage: luamerger [-o output.lua] [-m main.lua] SRC_DIRECTORY"
  parser.on "-h", "--help", "Shows this help" do
    puts parser
    exit
  end
  parser.on "-m SCRIPT", "--main=SCRIPT", "Use as main script" do |x|
    main_script = File.read main_script
  end
  parser.on "-o OUTPUT", "--out=OUTPUT", "Write result to file instead of stdout" { |x| output = File.open(x, "w") }
end
if ARGV.size != 1
  puts parser
  exit 1
end
directory = ARGV[0]

main_script ||= ""
merger = Merger.new

def add_modules(merger : Merger, main_file : String, dir : String, prefix : String? = nil)
  Dir.foreach dir do |file|
    relpath = File.join(dir, file)
    modpath = prefix ? File.join(prefix, file) : file
    next if {"..", "."}.includes? file
    if modpath == main_file
      merger.main = File.read relpath
      next
    end
    if Dir.exists? relpath
      add_modules merger, main_file, relpath, modpath
      next
    end
    merger.add_module Module.new name: modpath.sub(/.lua$/, "").gsub(/\\|\//, "."), content: File.read(relpath)
  end
end

add_modules merger, main_script, directory
merger.to_s output
output.close if output != STDOUT
