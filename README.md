# LuaMerge

Simple Lua file merger that lets you use limited `require` in restricted environments.  
Made for Binding of Isaac: Afterbirth+, but can be used pretty much everywhere.

## Building

    make

## Usage

    ./bin/luamerger --help

## Example

    cd example
    make
