luamerger: src/cli.cr src/merger.cr
	if ! [ -d bin ]; then mkdir bin; fi
	crystal build --release src/cli.cr -o bin/luamerger

all: luamerger
.PHONY: all
